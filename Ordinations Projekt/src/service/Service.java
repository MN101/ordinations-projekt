package service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import model.DagligFast;
import model.DagligSkaev;
import model.Laegemiddel;
import model.PN;
import model.Patient;
import storage.Storage;

public class Service {
    private static Storage storage;

    public Service(Storage storage) {
        Service.storage = storage;
    }

    public Service() {
        //
    }

    /**
     * Opretter og returnerer en PN ordination.
     * Hvis startDato er efter slutDato kastes en IllegalArgumentException,
     * og ordinationen oprettes ikke.
     * Pre: antal >= 0.
     */
    public PN opretPNOrdination(LocalDate startDen, LocalDate slutDen,
            Patient patient, Laegemiddel laegemiddel, double antal) {
        // TODO
        return null;
    }

    /**
     * Opretter og returnerer en DagligFast ordination.
     * Hvis startDato er efter slutDato kastes en IllegalArgumentException,
     * og ordinationen oprettes ikke.
     * Pre: Alle antal er >= 0 eller -1 (dvs. ikke sat).
     */
    public DagligFast opretDagligFastOrdination(LocalDate startDen,
            LocalDate slutDen, Patient patient, Laegemiddel laegemiddel,
            double morgenAntal, double middagAntal, double aftenAntal,
            double natAntal) {
        // TODO 
        return null;
    }

    /**
     * Opretter og returnerer en DagligSkæv ordination.
     * Hvis startDato er efter slutDato kastes en IllegalArgumentException,
     * og ordinationen oprettes ikke.
     * Hvis antallet af elementer i klokkeSlet og antalEnheder er forskellige
     * kastes en IllegalArgumentException.
     * Pre: Alle tal i antalEnheder er >= 0.
     */
    public DagligSkaev opretDagligSkaevOrdination(LocalDate startDen,
            LocalDate slutDen, Patient patient, Laegemiddel laegemiddel,
            LocalTime[] klokkeSlet, double[] antalEnheder) {
        // TODO
        return null;
    }

    /**
     * En dato for hvornår ordinationen anvendes tilføjes ordinationen.
     * Hvis datoen ikke er indenfor ordinationens gyldighedsperiode
     * kastes en IllegalArgumentException.
     */
    public void ordinationPNAnvendt(PN pn, LocalDate dato) {
        // TODo
    }

    /**.
     * Den anbefalede dosis for den pågældende patient 
     * (afhænger af patientens vægt). 
     */
    public double anbefaletDosisPrDoegn(Patient patient, Laegemiddel laegemiddel) {
        double result;
        if (patient.getVaegt() < 25) {
            result = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnLet();
        } else if (patient.getVaegt() > 120) {
            result = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnTung();
        } else {
            result = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnNormal();
        }
        return result;
    }

    /**
     * For et givent vægtinterval og et givent lægemiddel
     * returneres antallet af ordinationer.
     * Pre: 0 <= vægtStart <= vægtSlut.
     */
    public int antalOrdinationerPrVægtPrLægemiddel(double vaegtStart, double vaegtSlut,
            Laegemiddel laegemiddel) {
        // TODO
        return 0;
    }

    public List<Patient> getAllPatienter() {
        return storage.getAllPatienter();
    }

    public List<Laegemiddel> getAllLaegemidler() {
        return storage.getAllLaegemidler();
    }

    /**
     * Pre: navn ikke tomt, cpr ikke tomt, vaegt >= 0.
     */
    public Patient opretPatient(String cpr, String navn, double vaegt) {
        Patient p = new Patient(cpr, navn, vaegt);
        storage.addPatient(p);
        return p;
    }

    /**
     * Pre: navn ikke tomt, enhed ikke tomt.<br/>
     * Pre: enhedPrKgDoegnLet >= 0, enhedPrKgDoegnnormal >= 0, enhedPrKgDoegnTung >= 0.
     */
    public Laegemiddel opretLaegemiddel(String navn,
            double enhedPrKgPrDoegnLet, double enhedPrKgPrDoegnNormal,
            double enhedPrKgPrDoegnTung, String enhed) {
        assert !navn.isEmpty();
        assert enhedPrKgPrDoegnLet >= 0 && enhedPrKgPrDoegnNormal >= 0 && enhedPrKgPrDoegnTung >= 0;
        Laegemiddel lm = new Laegemiddel(navn,
                enhedPrKgPrDoegnLet, enhedPrKgPrDoegnNormal, enhedPrKgPrDoegnTung,
                enhed);
        storage.addLaegemiddel(lm);
        return lm;
    }

    public void createSomeObjects() {
        this.opretPatient("121256-0512", "Jane Jensen", 63.4);
        this.opretPatient("070985-1153", "Finn Madsen", 83.2);
        this.opretPatient("050972-1233", "Hans Jørgensen", 89.4);
        this.opretPatient("011064-1522", "Ulla Nielsen", 59.9);
        this.opretPatient("090149-2529", "Ib Hansen", 87.7);

        this.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
        this.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
        this.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
        this.opretLaegemiddel("Methotrexat", 0.01, 0.015, 0.02, "Styk");

        this.opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12),
                storage.getAllPatienter().get(0), storage.getAllLaegemidler().get(1),
                123);

        this.opretPNOrdination(LocalDate.of(2015, 2, 12), LocalDate.of(2015, 2, 14),
                storage.getAllPatienter().get(0), storage.getAllLaegemidler()
                        .get(0),
                3);

        this.opretPNOrdination(LocalDate.of(2015, 1, 20), LocalDate.of(2015, 1, 25),
                storage.getAllPatienter().get(3), storage.getAllLaegemidler()
                        .get(2),
                5);

        this.opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12),
                storage.getAllPatienter().get(0), storage.getAllLaegemidler()
                        .get(1),
                123);

        this.opretDagligFastOrdination(LocalDate.of(2015, 1, 10),
                LocalDate.of(2015, 1, 12), storage.getAllPatienter().get(1),
                storage.getAllLaegemidler().get(1), 2, -1, 1, -1);

        LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40),
                LocalTime.of(16, 0), LocalTime.of(18, 45) };
        double[] an = { 0.5, 1, 2.5, 3 };

        this.opretDagligSkaevOrdination(LocalDate.of(2015, 1, 23),
                LocalDate.of(2015, 1, 24), storage.getAllPatienter().get(1),
                storage.getAllLaegemidler().get(2), kl, an);
    }
}
