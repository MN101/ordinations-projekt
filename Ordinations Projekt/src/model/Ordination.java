package model;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public abstract class Ordination {
    private final LocalDate startDen;
    private final LocalDate slutDen;

    /** Pre: startDen <= slutDen. */
    public Ordination(LocalDate startDen, LocalDate slutDen) {
        assert startDen != null && slutDen != null;
        this.startDen = startDen;
        this.slutDen = slutDen;
    }

    public LocalDate getStartDen() {
        return startDen;
    }

    public LocalDate getSlutDen() {
        return slutDen;
    }

    /**
     * Returnerer antal hele dage mellem startdato og slutdato.
     * Begge dage inklusive.
     */
    public int antalDage() {
        return (int) ChronoUnit.DAYS.between(startDen, slutDen) + 1;
        // return startDen.until(slutDen, ChronoUnit.DAYS) + 1;
    }

    @Override
    public String toString() {
        return startDen.toString();
    }

    /**
     * Returnerer den totale dosis der er givet i den periode ordinationen er gyldig.
     */
    public abstract double samletDosis();

    /**
     * Returnerer den gennemsnitlige dosis givet pr dag i den periode ordinationen er gyldig.
     */
    public abstract double doegnDosis();

    /**
     * Returnerer ordinationstypen som en tekststreng.
     */
    public abstract String getType();

}
