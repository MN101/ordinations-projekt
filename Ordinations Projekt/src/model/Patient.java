package model;

public class Patient {
    private final String cprnr;
    private final String navn;
    private final double vaegt;

    /** Pre: cpnnr ikke tomt, navn ikke tomt, vaegt >= 0. */
    public Patient(String cprnr, String navn, double vaegt) {
        assert !cprnr.isEmpty();
        assert !navn.isEmpty();
        assert vaegt >= 0;
        this.cprnr = cprnr;
        this.navn = navn;
        this.vaegt = vaegt;
    }

    public String getCprnr() {
        return cprnr;
    }

    public String getNavn() {
        return navn;
    }

    public double getVaegt() {
        return vaegt;
    }

    @Override
    public String toString() {
        return navn + "  " + cprnr;
    }
}
