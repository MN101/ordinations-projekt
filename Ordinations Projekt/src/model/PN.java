package model;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class PN extends Ordination {

    private final double antalEnheder;
    private final ArrayList<LocalDate> ordinationsDatoer = new ArrayList<>();

    public PN(LocalDate startDen, LocalDate slutDen, double antalEnheder) {
        super(startDen, slutDen);
        assert antalEnheder >= 0;
        this.antalEnheder = antalEnheder;
    }

    //---------------------------------------------------------------------------

    /**
     * Finder laveste dato i ArrayListen OrdinationsDatoer og retunerer den
     * @return
     */
    private LocalDate findLavesteDato() {
        LocalDate temp = ordinationsDatoer.get(0);
        for (LocalDate ld : ordinationsDatoer) {
            if (ld.compareTo(temp) < 0) {
                temp = ld;
            }
        }
        return temp;
    }

    /**
     * Finder højeste dato i ArrayListen OrdinationsDatoer og retunerer den
     * @return
     */
    private LocalDate findHøjesteDato() {
        LocalDate temp = ordinationsDatoer.get(0);
        for (LocalDate ld : ordinationsDatoer) {
            if (ld.compareTo(temp) > 0) {
                temp = ld;
            }
        }
        return temp;
    }

    /**
     * Finder og retunerer antal dage mellem 2 LocalDate datoer
     * @return
     */
    private int findAntalDageMellem(LocalDate lav, LocalDate høj) {
        return (int) ChronoUnit.DAYS.between(lav, høj);
    }

    //---------------------------------------------------------------------------

    /**
     * Registrerer at der er givet en dosis paa dagen givesDen.
     * Returnerer true, hvis givesDen er inden for ordinationens gyldighedsperiode,
     * og datoen huskes.
     * Returnerer false ellers og datoen givesDen ignoreres.
     */

    public boolean givDosis(LocalDate givesDen) {

        if (super.getStartDen().isBefore(givesDen) && super.getSlutDen().isAfter(givesDen)) {
            ordinationsDatoer.add(givesDen);
            return true;
        } else {
            return false;
        }

    }

    /**
     * Returnerer antal gange ordinationen er anvendt.
     */
    public int getAntalGangeGivet() {
        return ordinationsDatoer.size();
    }

    public double getAntalEnheder() {
        return antalEnheder;
    }

    @Override
    public double samletDosis() {
        return getAntalGangeGivet() * antalEnheder;
    }

    @Override
    public double doegnDosis() {
        return getAntalGangeGivet() * antalEnheder / findAntalDageMellem(findLavesteDato(), findHøjesteDato());
    }

    @Override
    public String getType() {
        return "PN";
    }
}
